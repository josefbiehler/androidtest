package com.example.listviewtest1;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class Monatsadapter extends BaseAdapter {

	private List<Monat> monate;
	private LayoutInflater inflater;
	
	public Monatsadapter(Context context) {
		inflater = LayoutInflater.from(context);
		monate = new ArrayList<Monat>();
		
		for(int i = 0; i < 12; i++) {
			monate.add(new Monat(i, "adda"+i));
		}
	}
	@Override
	public int getCount() {
		
		return monate.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return monate.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		 ViewHolder holder = null;
		 
		 if (convertView == null) {
			 convertView = inflater.inflate(R.layout.item, parent, false);
			 holder = new ViewHolder();
			 holder.textView = (TextView)convertView.findViewById(R.id.monatTitle);
			 convertView.setTag(holder);
		 }else {
			 holder = (ViewHolder)convertView.getTag();
		 }
		 
		 Monat monat = (Monat)getItem(position);
		 holder.textView.setText(String.valueOf(monat.getMonat()));
		 
		 return convertView;
	}
	
	private class ViewHolder {
		TextView textView;
	}
}
