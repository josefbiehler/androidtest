package com.example.listviewtest1;

import java.io.Serializable;

public class Monat implements Serializable {
	private int monat;
	private String info;
	
	public Monat(int m, String i) {
		this.monat = m;
		info = i;
	}
	
	public int getMonat() {
		return monat;
	}
	
	public void setMonat(int m) {
		this.monat = m;
	}
	
	public String getInfo() {
		return info;
	}
	
	public void setInfo(String i) {
		info = i;
	}

}
