package com.example.listviewtest1;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

public class ItemActivity extends ListActivity implements OnItemClickListener {

	private Monatsadapter adapter;
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		Monat monat = (Monat) adapter.getItem(position);
		Intent intent = new Intent(this, ItemDetailActivity.class);
		intent.putExtra("item", monat);
		this.startActivity(intent);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		adapter = new Monatsadapter(this);
		setListAdapter(adapter);
		getListView().setOnItemClickListener(this);
	}
}
