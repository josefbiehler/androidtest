package com.example.listviewtest1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class ItemDetailActivity extends Activity {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.id);
		Intent i  = getIntent();
		TextView text = (TextView)findViewById(R.id.itemDetailText);
		Monat m = (Monat)i.getSerializableExtra("item");
		text.setText(m.getInfo());
	}
}
