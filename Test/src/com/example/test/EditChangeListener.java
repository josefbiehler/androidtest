package com.example.test;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;

public class EditChangeListener implements TextWatcher {

	private Button button;
	
	public EditChangeListener(Button button) {
		this.button = button;
	}
	
	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void afterTextChanged(Editable s) {
		if(s.length() > 0) {
			button.setEnabled(true);
		}else{
			button.setEnabled(false);
		}
	}

}
