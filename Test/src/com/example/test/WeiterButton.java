package com.example.test;
import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class WeiterButton implements OnClickListener {

	private TextView nachricht;
	private EditText eingabe;
	private boolean erster_klick;
	private Activity activity;
	private Button button;
	
	public WeiterButton(TextView nachricht, EditText eingabe, Activity activity, Button button) {
		this.nachricht = nachricht;
		this.eingabe = eingabe;
		this.activity = activity;
		this.button = button;
		erster_klick = false;
	}
	
	@Override
	public void onClick(View v) {
		if(!erster_klick) {
			this.eingabe.setVisibility(View.INVISIBLE);
			this.nachricht.setText(activity.getString(R.string.hallo, eingabe.getText()));
			button.setText(R.string.fertig);
			erster_klick = true;
		}else {
			activity.finish();
		}
	}

}
