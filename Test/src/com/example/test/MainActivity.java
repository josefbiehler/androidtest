package com.example.test;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {

	private TextView nachricht;
	private Button weiter_fertig;
	private EditText eingabe;
	
	private boolean erster_klick;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        nachricht = (TextView)findViewById(R.id.nachricht);
        weiter_fertig = (Button)findViewById(R.id.weiter_fertig);
        eingabe = (EditText)findViewById(R.id.eingabe);

        nachricht.setText(R.string.willkommen);
        weiter_fertig.setText(R.string.weiter);
        weiter_fertig.setOnClickListener(new WeiterButton(nachricht, eingabe, this, weiter_fertig));
        eingabe.addTextChangedListener(new EditChangeListener(weiter_fertig));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
}
