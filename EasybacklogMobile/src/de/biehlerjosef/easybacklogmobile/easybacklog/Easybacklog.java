package de.biehlerjosef.easybacklogmobile.easybacklog;
import java.util.List;

import de.biehlerjosef.easybacklogmobile.core.domain.*;
public interface Easybacklog {
	List<Backlog> getBacklogs();
}
