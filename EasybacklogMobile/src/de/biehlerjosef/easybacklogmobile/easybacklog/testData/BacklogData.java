package de.biehlerjosef.easybacklogmobile.easybacklog.testData;

import java.util.ArrayList;
import java.util.List;

import de.biehlerjosef.easybacklogmobile.core.domain.Backlog;

public class BacklogData {
	public static List<Backlog> getBacklogs() {
		List<Backlog> liste = new ArrayList();
		liste.add(new Backlog(){{setId(1); setName("Test 1");}});
		liste.add(new Backlog(){{setId(1); setName("Test 2");}});
		liste.add(new Backlog(){{setId(1); setName("Test 3");}});
		return liste;
	}
}
