package de.biehlerjosef.easybacklogmobile.easybacklog;

import java.util.List;

import de.biehlerjosef.easybacklogmobile.core.domain.Backlog;
import de.biehlerjosef.easybacklogmobile.easybacklog.testData.BacklogData;

public class EasybacklogImplementation implements Easybacklog {

	@Override
	public List<Backlog> getBacklogs() {
		return BacklogData.getBacklogs();
	}
	
}
