package de.biehlerjosef.easybacklogmobile.android;

import de.biehlerjosef.easybacklogmobile.R;
import de.biehlerjosef.easybacklogmobile.R.layout;
import de.biehlerjosef.easybacklogmobile.R.menu;
import de.biehlerjosef.easybacklogmobile.android.backlog.BacklogListActivity;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		findViewById(R.id.button1).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				start();
			}
			
		});
	}
	
	protected void start() {
		Intent intent = new Intent(this, BacklogListActivity.class);
		startActivity(intent);
	}
}
