package de.biehlerjosef.easybacklogmobile.android.backlog;
import java.util.ArrayList;
import java.util.List;

import de.biehlerjosef.easybacklogmobile.R;
import de.biehlerjosef.easybacklogmobile.core.domain.Backlog;
import de.biehlerjosef.easybacklogmobile.easybacklog.EasybacklogImplementation;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class BacklogDetailItemAdapter extends BaseAdapter {

	private List<Backlog> backlogs;
	private Context context;
	private LayoutInflater inflater;
	
	public BacklogDetailItemAdapter(Context context) {
		this.context = context;
		inflater = LayoutInflater.from(context);
		backlogs = new ArrayList();
		EasybacklogImplementation eb = new EasybacklogImplementation();
		backlogs = eb.getBacklogs();
	}
	
	@Override
	public int getCount() {
		return backlogs.size();
	}

	@Override
	public Object getItem(int position) {
		return backlogs.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.backlogdetail, parent, false);
			holder =  new ViewHolder();
			holder.name = (TextView)convertView.findViewById(R.id.backlogdetail_backlog_name);
			convertView.setTag(holder);
		}else{
			holder = (ViewHolder)convertView.getTag();
		}
		
		Backlog backlog = (Backlog)getItem(position);
		holder.name.setText(backlog.getName());
		return convertView;
	}

	private class ViewHolder {
		public TextView name;
	}
}
