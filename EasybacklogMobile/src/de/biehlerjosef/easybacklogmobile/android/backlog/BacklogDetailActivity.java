package de.biehlerjosef.easybacklogmobile.android.backlog;

import de.biehlerjosef.easybacklogmobile.R;
import de.biehlerjosef.easybacklogmobile.core.domain.Backlog;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;

public class BacklogDetailActivity extends Activity {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.backlogdetail);
		Intent intent = getIntent();
		Backlog backlog = (Backlog)intent.getSerializableExtra("backlog");
		((TextView)findViewById(R.id.backlogdetail_backlog_name)).setText(backlog.getName());
	}
}
