package de.biehlerjosef.easybacklogmobile.android.backlog;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import de.biehlerjosef.easybacklogmobile.core.domain.Backlog;

public class BacklogListActivity extends ListActivity implements OnItemClickListener {
	BacklogDetailItemAdapter adapter;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		adapter = new BacklogDetailItemAdapter(this);
		setListAdapter(adapter);
		getListView().setOnItemClickListener(this);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		Backlog backlog = (Backlog)adapter.getItem(position);
		Intent intent = new Intent(this, BacklogDetailActivity.class);
		intent.putExtra("backlog", backlog);
		startActivity(intent);
	}
}
