package de.biehlerjosef.easybacklogmobile.core.domain;

public abstract class AbstractEasybacklogObject extends AbstractDomainObject {
	private Integer eId;
	
	public Integer geteId() {
		return eId;
	}
	public void seteId(Integer eId) {
		this.eId = eId;
	}

	@Override
	public boolean equals(Object o) {
		if(this == o) {
			return true;
		}
		if(this == null) {
			return false;
		}

		if(!getClass().equals(o)) {
			return false;
		}
		
		return ((AbstractEasybacklogObject)o).eId.equals(this.eId);
	}
	
	@Override
	public int hashCode() {
		return eId.hashCode();
	}
}
