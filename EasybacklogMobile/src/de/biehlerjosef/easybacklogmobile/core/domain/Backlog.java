package de.biehlerjosef.easybacklogmobile.core.domain;

import java.io.Serializable;

public class Backlog extends AbstractEasybacklogObject implements Serializable {
	private String name;
	
	public void setName(String val) {
		name = val;
	}
	
	public String getName() {
		return name;
	}
}
