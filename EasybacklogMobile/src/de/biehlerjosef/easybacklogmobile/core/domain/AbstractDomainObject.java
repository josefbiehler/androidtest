package de.biehlerjosef.easybacklogmobile.core.domain;

public abstract class AbstractDomainObject {
	private Integer id;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Override
	public boolean equals(Object o) {
		if(this == o) {
			return true;
		}
		if(this == null) {
			return false;
		}

		if(!getClass().equals(o)) {
			return false;
		}
		
		return ((AbstractDomainObject)o).id.equals(this.id);
	}
	
	@Override
	public int hashCode() {
		return id;
	}
}
